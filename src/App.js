import { useState, useRef } from 'react';
import './App.css';

const dateComponentBase = 'date-component';
const dateComponentNames = { day: 'day', month: 'month', year: 'year' };
const dateComponentFullNames = Object.keys(dateComponentNames).reduce((dict, key) => {
  dict[key] = dateComponentBase.concat('-', key);
  return dict;
}, {});

const requiredFieldMessage = "This field is required";
const mustBeValidBase = "Must be a valid";
const mustBeValid = Object.keys(dateComponentNames).reduce((dict, key) => {
  dict[key] = mustBeValidBase.concat(' ', key);
  return dict;
}, {});
const mustBeInThePast = "Must be in the past";


function CustomInput({ buttonRef, errors, inputDate, setInputDate, labelText, inputPlaceholder, dateComponentNameKey }) {
  function HandleInputChange(event) {
    console.log(event.target.value);
    setInputDate({ ...inputDate, [event.target.dataset.componentName]: event.target.value });
  }

  function HandleKeyDown(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      buttonRef.current.dispatchEvent(new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window,
      }));
    }
  }

  const error = errors[dateComponentNameKey]

  return (
    <div className="date-component-container">
      <label className={"description" + (error ? " error" : "")}>{labelText.toUpperCase()}</label>
      <input
        type="text"
        value={inputDate[dateComponentNameKey] ?? ''}
        className={dateComponentFullNames[dateComponentNameKey]}
        placeholder={inputPlaceholder.toUpperCase()}
        onChange={HandleInputChange}
        onKeyDown={HandleKeyDown}
        data-component-name={dateComponentNameKey}
      />
      {error &&
      <label className="error error-description">{error}</label>
      }
    </div>
  );
}

function Output({ value, labelText }) {
  return (
    <div className="output-portion">
      <label className="output-number">{value ?? '--'}</label>
      <label className="output-number-unit">&nbsp;{labelText}</label>
    </div>
  );
}  

function App() {
  const [inputDate, setInputDate] = useState({ day: '', month: '', year: '' });
  const [age, setAge] = useState({ days: null, months: null, years: null });
  const [errors, setErrors] = useState({ day: null, month: null, year: null });
  const buttonRef = useRef(null);

  function GetDateInterval(start, end) {
    let years = end.getFullYear() - start.getFullYear();
    let months = end.getMonth() - start.getMonth();
    let days = end.getDate() - start.getDate();
  
    // Adjust for negative day internal
    if (days < 0) {
      days += new Date(years, months, 0).getDate();
      months -= 1;
    }
    // Adjust for negative month or day interval
    if (months < 0 || (months === 0 && days < 0)) {
      years--;
      if (months < 0) months += 12;
      if (days < 0) {
        const monthDays = new Date(
          start.getFullYear(),
          start.getMonth() + 1,
          0
        ).getDate();
        days += monthDays;
        months--;
      }
    }
  
    return { years, months, days };
  }

  function ValidateAndCalculateAge(day, month, year, setErrors) {
    let newErrors = { day: null, month: null, year: null };
    const date = new Date(year, month - 1, day), now = new Date();

    // Year
    if (year === '') {
      newErrors.year = requiredFieldMessage;
    } else if (isNaN(year) || year < 1) {
      newErrors.year = mustBeValid.year;
    } else {
      date.setFullYear(year);
    }
  
    // Month
    if (month === '') {
      newErrors.month = requiredFieldMessage;
    } else if (isNaN(month) || month < 1 || month > 12) {
      newErrors.month = mustBeValid.month;
    }

    // Day
    if (day === '') {
      newErrors.day = requiredFieldMessage;
    } else if (isNaN(day) || day < 1 || day > new Date(year, month, 0).getDate()) {
      newErrors.day = mustBeValid.day;
    }

    if (now < date) {
      newErrors.year = mustBeInThePast;
    }

    setErrors(newErrors);

    return Object.values(newErrors).some(value => value !== null) ? null : GetDateInterval(date, now);
  }
  
  function UpdateDate() {
    const newAge = ValidateAndCalculateAge(inputDate.day, inputDate.month, inputDate.year, setErrors);
    if (newAge !== null) {
      setAge(newAge);
    }
  }
  
  return (
    <div className="app">
      <div className="date-input">
        <CustomInput
          buttonRef={buttonRef}
          errors={errors}
          inputDate={inputDate}
          setInputDate={setInputDate}
          labelText="Day"
          inputPlaceholder="DD"
          dateComponentNameKey="day"
        />
        <CustomInput
          buttonRef={buttonRef}
          errors={errors}
          inputDate={inputDate}
          setInputDate={setInputDate}
          labelText="Month"
          inputPlaceholder="MM"
          dateComponentNameKey="month"
        />
        <CustomInput
          buttonRef={buttonRef}
          errors={errors}
          inputDate={inputDate}
          setInputDate={setInputDate}
          labelText="Year"
          inputPlaceholder="YYYY"
          dateComponentNameKey="year"
        />
      </div>
      <div className="divider">
        <div className="arrow non-selectable" onClick={UpdateDate} ref={buttonRef}>
          <img src="/icon-arrow.svg" alt="SVG icon"/>
        </div>
        <hr/>
      </div>
      <div className="result">
        <Output
          value={age.years}
          labelText="years"
        />
        <Output
          value={age.months}
          labelText="months"
        />
        <Output
          value={age.days}
          labelText="days"
        />
      </div>
    </div>
  );
}

export default App;
